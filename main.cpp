#include <iostream>
#include "code.h"
#include "token.h"
#include "flowoftokens.h"
#include "numberscanner.h"
#include "stringscanner.h"
#include "operationscanner.h"
#include "identificatorscanner.h"
#include "parenthesisscanner.h"
#include "ascii.h"

int main(){
	Code code("code.lsp");
	NumberScanner numbers(&code);
	StringScanner strings(&code);
	OperationScanner operations(&code);
	ParenthesisScanner parenthesis(&code);
	IdentificatorScanner identificators(&code);
	FlowOfTokens TokensFlow;
	while(!code.IsEnd()){
		char ch = code.ShowCh();
		if(((int)ch >= ZERO_ASCII && (int)ch <= NINE_ASCII)){
			TokensFlow.addToken(numbers.getToken());
		} else if (ch == '\"'){
			TokensFlow.addToken(strings.getToken());
		} else if(((int)ch>=a_ASCII && (int)ch<=z_ASCII) || ((int)ch>=A_ASCII && (int)ch<=Z_ASCII) || ch == '_'){
			TokensFlow.addToken(identificators.getToken());
		} else if (ch == '+' || ch == '-' || ch == '*' || ch == '/'){
			TokensFlow.addToken(operations.getToken());
		} else if (ch =='(' || ch == ')'){
			TokensFlow.addToken(parenthesis.getToken());
		} else if(ch == ' ' || ch == '\n' || ch == '\t'){
            code.GiveCh();
		}
    }
	while (!TokensFlow.IsEnd()){
		Token token = TokensFlow.showToken();
		std::cout<<token.getStringNumber()<<"\tⱡ\t"<<token.getValue()<<"\tⱡ\t"<<token.getClassNumber()<<"\tⱡ\t"
				 <<token.getSubClassNumber()<<std::endl;
		TokensFlow.getToken();
	}
    return 0;
}

