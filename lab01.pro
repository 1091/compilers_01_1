TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    code.cpp \
    numberscanner.cpp \
    token.cpp \
    flowoftokens.cpp \
    stringscanner.cpp \
    operationscanner.cpp \
    parenthesisscanner.cpp \
    identificatorscanner.cpp

HEADERS += code.h \
    token.h \
    numberscanner.h \
    flowoftokens.h \
    stringscanner.h \
    classes.h \
    ascii.h \
    operationscanner.h \
    parenthesisscanner.h \
    identificatorscanner.h

QMAKE_CXXFLAGS += -std=c++11

