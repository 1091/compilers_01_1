#include "token.h"

Token::Token(int StringNumber, int ClassNumber, int SubClassNumber, std::string Value){
	this->StringNumber = StringNumber;
	this->ClassNumber = ClassNumber;
	this->SubClassNumber = SubClassNumber;
	this->Value = Value;
}

Token::Token(int StringNumber, int ClassNumber, std::string Value){
	this->StringNumber = StringNumber;
	this->ClassNumber = ClassNumber;
	this->SubClassNumber = 0;
	this->Value = Value;
}

Token::Token(){
	this->StringNumber = 0;
	this->ClassNumber = 0;
	this->SubClassNumber = 0;
	this->Value = "";
}

int Token::getClassNumber(){
	return ClassNumber;
}

int Token::getStringNumber(){
	return StringNumber;
}

int Token::getSubClassNumber(){
	return SubClassNumber;
}

std::string Token::getValue(){
	return Value;
}

